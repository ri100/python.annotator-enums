from setuptools import setup

setup(
    name='annotator_enums',
    version='1.0',
    description='Annotator static keys',
    author='Risto Kowaczewski',
    author_email='risto.kowaczewski@gmail.com',
    packages=['annotator_enums']
)
